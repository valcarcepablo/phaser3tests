

export function getDucksJson() {
    return  {
        "textures": [
            {
                "image": "ducks.png",
                "format": "RGBA8888",
                "size": {
                    "w": 478,
                    "h": 323
                },
                "scale": 1,
                "frames": [
                    {
                        "filename": "duckmaster-duck 1-000.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 116,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 116,
                            "h": 111
                        },
                        "frame": {
                            "x": 1,
                            "y": 1,
                            "w": 116,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-001.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 100,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 100,
                            "h": 111
                        },
                        "frame": {
                            "x": 119,
                            "y": 1,
                            "w": 100,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-002.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 80,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 80,
                            "h": 111
                        },
                        "frame": {
                            "x": 221,
                            "y": 1,
                            "w": 80,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-003.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 60,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 60,
                            "h": 111
                        },
                        "frame": {
                            "x": 303,
                            "y": 1,
                            "w": 60,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-004.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 111
                        },
                        "frame": {
                            "x": 365,
                            "y": 1,
                            "w": 30,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-005.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 10,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 10,
                            "h": 111
                        },
                        "frame": {
                            "x": 397,
                            "y": 1,
                            "w": 10,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-006.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 10,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 10,
                            "h": 111
                        },
                        "frame": {
                            "x": 409,
                            "y": 1,
                            "w": 10,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-007.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 111
                        },
                        "frame": {
                            "x": 421,
                            "y": 1,
                            "w": 30,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-008.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 60,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 60,
                            "h": 111
                        },
                        "frame": {
                            "x": 1,
                            "y": 114,
                            "w": 60,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-009.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 80,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 80,
                            "h": 111
                        },
                        "frame": {
                            "x": 63,
                            "y": 114,
                            "w": 80,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-010.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 100,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 100,
                            "h": 111
                        },
                        "frame": {
                            "x": 145,
                            "y": 114,
                            "w": 100,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 1-011.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 116,
                            "h": 111
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 116,
                            "h": 111
                        },
                        "frame": {
                            "x": 247,
                            "y": 114,
                            "w": 116,
                            "h": 111
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-000.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 100,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 100,
                            "h": 95
                        },
                        "frame": {
                            "x": 365,
                            "y": 114,
                            "w": 100,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-001.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 80,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 80,
                            "h": 95
                        },
                        "frame": {
                            "x": 1,
                            "y": 227,
                            "w": 80,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-002.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 60,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 60,
                            "h": 95
                        },
                        "frame": {
                            "x": 83,
                            "y": 227,
                            "w": 60,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-003.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 95
                        },
                        "frame": {
                            "x": 145,
                            "y": 227,
                            "w": 30,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-004.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 10,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 10,
                            "h": 95
                        },
                        "frame": {
                            "x": 177,
                            "y": 227,
                            "w": 10,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-005.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 10,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 10,
                            "h": 95
                        },
                        "frame": {
                            "x": 189,
                            "y": 227,
                            "w": 10,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-006.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 95
                        },
                        "frame": {
                            "x": 201,
                            "y": 227,
                            "w": 30,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-007.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 60,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 60,
                            "h": 95
                        },
                        "frame": {
                            "x": 233,
                            "y": 227,
                            "w": 60,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-008.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 80,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 80,
                            "h": 95
                        },
                        "frame": {
                            "x": 295,
                            "y": 227,
                            "w": 80,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duckmaster-duck 2-009.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 100,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 100,
                            "h": 95
                        },
                        "frame": {
                            "x": 377,
                            "y": 227,
                            "w": 100,
                            "h": 95
                        }
                    }
                ]
            }
        ],
        "meta": {
            "app": "https://www.codeandweb.com/texturepacker",
            "version": "3.0",
            "smartupdate": "$TexturePacker:SmartUpdate:a7ea048cbb241ec53f14ba4689e8bf09:4d08c51154f26138ced3fcfb36c9235d:bb64d8726c974dcac7e96a5895371ba9$"
        }
    }
    
}



export function getObjectsJson(){
    return {
        "textures": [
            {
                "image": "objects",
                "format": "RGBA8888",
                "size": {
                    "w": 292,
                    "h": 2046
                },
                "scale": 1,
                "frames": [
                    {
                        "filename": "rifle_red.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 141,
                            "h": 319
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 141,
                            "h": 319
                        },
                        "frame": {
                            "x": 2,
                            "y": 2,
                            "w": 141,
                            "h": 319
                        }
                    },
                    {
                        "filename": "rifle.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 319
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 319
                        },
                        "frame": {
                            "x": 2,
                            "y": 325,
                            "w": 142,
                            "h": 319
                        }
                    },
                    {
                        "filename": "target_red2_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 147,
                            "y": 2,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_colored_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 147,
                            "y": 148,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_red1_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 2,
                            "y": 648,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_red3_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 2,
                            "y": 794,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_white_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 2,
                            "y": 940,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_back_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 142,
                            "h": 142
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 142,
                            "h": 142
                        },
                        "frame": {
                            "x": 2,
                            "y": 1086,
                            "w": 142,
                            "h": 142
                        }
                    },
                    {
                        "filename": "target_back.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1232,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "target_white.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1364,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "target_red3.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1496,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "target_red2.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1628,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "target_red1.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1760,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "target_colored.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 128,
                            "h": 128
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 128,
                            "h": 128
                        },
                        "frame": {
                            "x": 2,
                            "y": 1892,
                            "w": 128,
                            "h": 128
                        }
                    },
                    {
                        "filename": "stick_wood_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 34,
                            "h": 127
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 34,
                            "h": 127
                        },
                        "frame": {
                            "x": 134,
                            "y": 1232,
                            "w": 34,
                            "h": 127
                        }
                    },
                    {
                        "filename": "stick_woodFixed_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 34,
                            "h": 127
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 34,
                            "h": 127
                        },
                        "frame": {
                            "x": 134,
                            "y": 1363,
                            "w": 34,
                            "h": 127
                        }
                    },
                    {
                        "filename": "stick_metal_outline.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 34,
                            "h": 127
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 34,
                            "h": 127
                        },
                        "frame": {
                            "x": 134,
                            "y": 1494,
                            "w": 34,
                            "h": 127
                        }
                    },
                    {
                        "filename": "stick_wood.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 123
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 123
                        },
                        "frame": {
                            "x": 134,
                            "y": 1625,
                            "w": 30,
                            "h": 123
                        }
                    },
                    {
                        "filename": "stick_woodFixed.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 123
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 123
                        },
                        "frame": {
                            "x": 134,
                            "y": 1752,
                            "w": 30,
                            "h": 123
                        }
                    },
                    {
                        "filename": "stick_metal.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 123
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 123
                        },
                        "frame": {
                            "x": 134,
                            "y": 1879,
                            "w": 30,
                            "h": 123
                        }
                    },
                    {
                        "filename": "duck_outline_target_brown.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 168,
                            "y": 1625,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_white.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 168,
                            "y": 1738,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_target_white.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 168,
                            "y": 1851,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_back.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 148,
                            "y": 294,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_brown.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 148,
                            "y": 407,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_target_yellow.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 148,
                            "y": 520,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_outline_yellow.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 114,
                            "h": 109
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 114,
                            "h": 109
                        },
                        "frame": {
                            "x": 148,
                            "y": 633,
                            "w": 114,
                            "h": 109
                        }
                    },
                    {
                        "filename": "duck_back.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 148,
                            "y": 746,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_target_brown.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 148,
                            "y": 845,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_target_white.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 148,
                            "y": 944,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_brown.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 148,
                            "y": 1043,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_target_yellow.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 172,
                            "y": 1142,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_white.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 172,
                            "y": 1241,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "duck_yellow.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 99,
                            "h": 95
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 99,
                            "h": 95
                        },
                        "frame": {
                            "x": 172,
                            "y": 1340,
                            "w": 99,
                            "h": 95
                        }
                    },
                    {
                        "filename": "stick_wood_outline_broken.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 18,
                            "h": 63
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 18,
                            "h": 63
                        },
                        "frame": {
                            "x": 148,
                            "y": 1142,
                            "w": 18,
                            "h": 63
                        }
                    },
                    {
                        "filename": "stick_metal_outline_broken.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 18,
                            "h": 63
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 18,
                            "h": 63
                        },
                        "frame": {
                            "x": 168,
                            "y": 1964,
                            "w": 18,
                            "h": 63
                        }
                    },
                    {
                        "filename": "stick_metal_broken.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 14,
                            "h": 59
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 14,
                            "h": 59
                        },
                        "frame": {
                            "x": 190,
                            "y": 1964,
                            "w": 14,
                            "h": 59
                        }
                    },
                    {
                        "filename": "stick_wood_broken.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 14,
                            "h": 59
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 14,
                            "h": 59
                        },
                        "frame": {
                            "x": 208,
                            "y": 1964,
                            "w": 14,
                            "h": 59
                        }
                    },
                    {
                        "filename": "shot_yellow_large.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 30
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 30
                        },
                        "frame": {
                            "x": 134,
                            "y": 2006,
                            "w": 30,
                            "h": 30
                        }
                    },
                    {
                        "filename": "shot_blue_large.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 30
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 30
                        },
                        "frame": {
                            "x": 226,
                            "y": 1964,
                            "w": 30,
                            "h": 30
                        }
                    },
                    {
                        "filename": "shot_grey_large.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 30
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 30
                        },
                        "frame": {
                            "x": 226,
                            "y": 1998,
                            "w": 30,
                            "h": 30
                        }
                    },
                    {
                        "filename": "shot_brown_large.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 30
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 30
                        },
                        "frame": {
                            "x": 260,
                            "y": 1964,
                            "w": 30,
                            "h": 30
                        }
                    },
                    {
                        "filename": "shot_brown_small.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 20,
                            "h": 20
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 20,
                            "h": 20
                        },
                        "frame": {
                            "x": 2,
                            "y": 2024,
                            "w": 20,
                            "h": 20
                        }
                    },
                    {
                        "filename": "shot_blue_small.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 20,
                            "h": 20
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 20,
                            "h": 20
                        },
                        "frame": {
                            "x": 26,
                            "y": 2024,
                            "w": 20,
                            "h": 20
                        }
                    },
                    {
                        "filename": "shot_grey_small.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 20,
                            "h": 20
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 20,
                            "h": 20
                        },
                        "frame": {
                            "x": 50,
                            "y": 2024,
                            "w": 20,
                            "h": 20
                        }
                    },
                    {
                        "filename": "shot_yellow_small.png",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 20,
                            "h": 20
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 20,
                            "h": 20
                        },
                        "frame": {
                            "x": 74,
                            "y": 2024,
                            "w": 20,
                            "h": 20
                        }
                    }
                ]
            }
        ],
        "meta": {
            "app": "http://free-tex-packer.com",
            "version": "0.6.7"
        }
    }
}
export function getStallJson(){
    return {
        "textures": [
            {
                "image": "Hud",
                "format": "RGBA8888",
                "size": {
                    "w": 791,
                    "h": 849
                },
                "scale": 1,
                "frames": [
                    {
                        "filename": "bg_blue",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 256,
                            "h": 256
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 256,
                            "h": 256
                        },
                        "frame": {
                            "x": 2,
                            "y": 2,
                            "w": 256,
                            "h": 256
                        }
                    },
                    {
                        "filename": "bg_red",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 256,
                            "h": 256
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 256,
                            "h": 256
                        },
                        "frame": {
                            "x": 262,
                            "y": 2,
                            "w": 256,
                            "h": 256
                        }
                    },
                    {
                        "filename": "bg_wood",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 256,
                            "h": 256
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 256,
                            "h": 256
                        },
                        "frame": {
                            "x": 2,
                            "y": 262,
                            "w": 256,
                            "h": 256
                        }
                    },
                    {
                        "filename": "bg_green",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 256,
                            "h": 256
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 256,
                            "h": 256
                        },
                        "frame": {
                            "x": 262,
                            "y": 262,
                            "w": 256,
                            "h": 256
                        }
                    },
                    {
                        "filename": "curtain",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 131,
                            "h": 426
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 131,
                            "h": 426
                        },
                        "frame": {
                            "x": 522,
                            "y": 2,
                            "w": 131,
                            "h": 426
                        }
                    },
                    {
                        "filename": "tree_oak",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 143,
                            "h": 244
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 143,
                            "h": 244
                        },
                        "frame": {
                            "x": 2,
                            "y": 522,
                            "w": 143,
                            "h": 244
                        }
                    },
                    {
                        "filename": "tree_pine",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 119,
                            "h": 255
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 119,
                            "h": 255
                        },
                        "frame": {
                            "x": 522,
                            "y": 432,
                            "w": 119,
                            "h": 255
                        }
                    },
                    {
                        "filename": "water1",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 132,
                            "h": 224
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 132,
                            "h": 224
                        },
                        "frame": {
                            "x": 149,
                            "y": 522,
                            "w": 132,
                            "h": 224
                        }
                    },
                    {
                        "filename": "water2",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 132,
                            "h": 223
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 132,
                            "h": 223
                        },
                        "frame": {
                            "x": 285,
                            "y": 522,
                            "w": 132,
                            "h": 223
                        }
                    },
                    {
                        "filename": "grass2",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 132,
                            "h": 216
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 132,
                            "h": 216
                        },
                        "frame": {
                            "x": 645,
                            "y": 432,
                            "w": 132,
                            "h": 216
                        }
                    },
                    {
                        "filename": "grass1",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 132,
                            "h": 200
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 132,
                            "h": 200
                        },
                        "frame": {
                            "x": 657,
                            "y": 2,
                            "w": 132,
                            "h": 200
                        }
                    },
                    {
                        "filename": "curtain_straight",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 256,
                            "h": 80
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 256,
                            "h": 80
                        },
                        "frame": {
                            "x": 285,
                            "y": 749,
                            "w": 256,
                            "h": 80
                        }
                    },
                    {
                        "filename": "curtain_top",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 200,
                            "h": 63
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 200,
                            "h": 63
                        },
                        "frame": {
                            "x": 2,
                            "y": 770,
                            "w": 200,
                            "h": 63
                        }
                    },
                    {
                        "filename": "cloud2",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 141,
                            "h": 84
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 141,
                            "h": 84
                        },
                        "frame": {
                            "x": 645,
                            "y": 652,
                            "w": 141,
                            "h": 84
                        }
                    },
                    {
                        "filename": "cloud1",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 134,
                            "h": 82
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 134,
                            "h": 82
                        },
                        "frame": {
                            "x": 545,
                            "y": 740,
                            "w": 134,
                            "h": 82
                        }
                    },
                    {
                        "filename": "curtain_rope",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 40,
                            "h": 21
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 40,
                            "h": 21
                        },
                        "frame": {
                            "x": 545,
                            "y": 826,
                            "w": 40,
                            "h": 21
                        }
                    }
                ]
            }
        ],
        "meta": {
            "app": "http://free-tex-packer.com",
            "version": "0.6.7"
        }
    }
}
export function getHudJson(){
    return {
        "textures": [
            {
                "image": "Hud",
                "format": "RGBA8888",
                "size": {
                    "w": 436,
                    "h": 444
                },
                "scale": 1,
                "frames": [
                    {
                        "filename": "text_gameover",
                        "rotated": false,
                        "trimmed": true,
                        "sourceSize": {
                            "w": 349,
                            "h": 72
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 348,
                            "h": 71
                        },
                        "frame": {
                            "x": 2,
                            "y": 2,
                            "w": 348,
                            "h": 71
                        }
                    },
                    {
                        "filename": "text_timeup",
                        "rotated": false,
                        "trimmed": true,
                        "sourceSize": {
                            "w": 295,
                            "h": 69
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 294,
                            "h": 69
                        },
                        "frame": {
                            "x": 2,
                            "y": 77,
                            "w": 294,
                            "h": 69
                        }
                    },
                    {
                        "filename": "text_ready",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 230,
                            "h": 66
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 230,
                            "h": 66
                        },
                        "frame": {
                            "x": 2,
                            "y": 150,
                            "w": 230,
                            "h": 66
                        }
                    },
                    {
                        "filename": "text_score",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 193,
                            "h": 64
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 193,
                            "h": 64
                        },
                        "frame": {
                            "x": 2,
                            "y": 220,
                            "w": 193,
                            "h": 64
                        }
                    },
                    {
                        "filename": "text_score_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 116,
                            "h": 39
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 116,
                            "h": 39
                        },
                        "frame": {
                            "x": 199,
                            "y": 220,
                            "w": 116,
                            "h": 39
                        }
                    },
                    {
                        "filename": "text_go",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 111,
                            "h": 66
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 111,
                            "h": 66
                        },
                        "frame": {
                            "x": 236,
                            "y": 150,
                            "w": 111,
                            "h": 66
                        }
                    },
                    {
                        "filename": "text_8",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 45,
                            "h": 58
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 45,
                            "h": 58
                        },
                        "frame": {
                            "x": 300,
                            "y": 77,
                            "w": 45,
                            "h": 58
                        }
                    },
                    {
                        "filename": "text_9",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 44,
                            "h": 57
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 44,
                            "h": 57
                        },
                        "frame": {
                            "x": 2,
                            "y": 288,
                            "w": 44,
                            "h": 57
                        }
                    },
                    {
                        "filename": "text_5",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 42,
                            "h": 57
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 42,
                            "h": 57
                        },
                        "frame": {
                            "x": 50,
                            "y": 288,
                            "w": 42,
                            "h": 57
                        }
                    },
                    {
                        "filename": "text_0",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 47,
                            "h": 57
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 47,
                            "h": 57
                        },
                        "frame": {
                            "x": 96,
                            "y": 288,
                            "w": 47,
                            "h": 57
                        }
                    },
                    {
                        "filename": "text_4",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 45,
                            "h": 56
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 45,
                            "h": 56
                        },
                        "frame": {
                            "x": 147,
                            "y": 288,
                            "w": 45,
                            "h": 56
                        }
                    },
                    {
                        "filename": "text_3",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 41,
                            "h": 56
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 41,
                            "h": 56
                        },
                        "frame": {
                            "x": 196,
                            "y": 288,
                            "w": 41,
                            "h": 56
                        }
                    },
                    {
                        "filename": "text_2",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 43,
                            "h": 56
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 43,
                            "h": 56
                        },
                        "frame": {
                            "x": 241,
                            "y": 263,
                            "w": 43,
                            "h": 56
                        }
                    },
                    {
                        "filename": "text_1",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 32,
                            "h": 56
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 32,
                            "h": 56
                        },
                        "frame": {
                            "x": 288,
                            "y": 263,
                            "w": 32,
                            "h": 56
                        }
                    },
                    {
                        "filename": "text_6",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 39,
                            "h": 56
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 39,
                            "h": 56
                        },
                        "frame": {
                            "x": 2,
                            "y": 349,
                            "w": 39,
                            "h": 56
                        }
                    },
                    {
                        "filename": "text_7",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 44,
                            "h": 55
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 44,
                            "h": 55
                        },
                        "frame": {
                            "x": 45,
                            "y": 349,
                            "w": 44,
                            "h": 55
                        }
                    },
                    {
                        "filename": "crosshair_outline_large",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 54,
                            "h": 54
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 54,
                            "h": 54
                        },
                        "frame": {
                            "x": 93,
                            "y": 349,
                            "w": 54,
                            "h": 54
                        }
                    },
                    {
                        "filename": "crosshair_white_large",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 50,
                            "h": 50
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 50,
                            "h": 50
                        },
                        "frame": {
                            "x": 151,
                            "y": 348,
                            "w": 50,
                            "h": 50
                        }
                    },
                    {
                        "filename": "crosshair_blue_large",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 50,
                            "h": 50
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 50,
                            "h": 50
                        },
                        "frame": {
                            "x": 205,
                            "y": 348,
                            "w": 50,
                            "h": 50
                        }
                    },
                    {
                        "filename": "crosshair_red_large",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 50,
                            "h": 50
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 50,
                            "h": 50
                        },
                        "frame": {
                            "x": 259,
                            "y": 323,
                            "w": 50,
                            "h": 50
                        }
                    },
                    {
                        "filename": "text_cross",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 45,
                            "h": 46
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 45,
                            "h": 46
                        },
                        "frame": {
                            "x": 354,
                            "y": 2,
                            "w": 45,
                            "h": 46
                        }
                    },
                    {
                        "filename": "crosshair_outline_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 46,
                            "h": 46
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 46,
                            "h": 46
                        },
                        "frame": {
                            "x": 351,
                            "y": 77,
                            "w": 46,
                            "h": 46
                        }
                    },
                    {
                        "filename": "icon_bullet_gold_long",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 44
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 44
                        },
                        "frame": {
                            "x": 351,
                            "y": 127,
                            "w": 21,
                            "h": 44
                        }
                    },
                    {
                        "filename": "icon_bullet_silver_long",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 44
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 44
                        },
                        "frame": {
                            "x": 376,
                            "y": 127,
                            "w": 21,
                            "h": 44
                        }
                    },
                    {
                        "filename": "icon_bullet_empty_long",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 44
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 44
                        },
                        "frame": {
                            "x": 351,
                            "y": 175,
                            "w": 21,
                            "h": 44
                        }
                    },
                    {
                        "filename": "crosshair_blue_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 42,
                            "h": 42
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 42,
                            "h": 42
                        },
                        "frame": {
                            "x": 324,
                            "y": 223,
                            "w": 42,
                            "h": 42
                        }
                    },
                    {
                        "filename": "crosshair_red_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 42,
                            "h": 42
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 42,
                            "h": 42
                        },
                        "frame": {
                            "x": 324,
                            "y": 269,
                            "w": 42,
                            "h": 42
                        }
                    },
                    {
                        "filename": "crosshair_white_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 42,
                            "h": 42
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 42,
                            "h": 42
                        },
                        "frame": {
                            "x": 324,
                            "y": 315,
                            "w": 42,
                            "h": 42
                        }
                    },
                    {
                        "filename": "text_dots",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 23,
                            "h": 39
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 23,
                            "h": 39
                        },
                        "frame": {
                            "x": 376,
                            "y": 175,
                            "w": 23,
                            "h": 39
                        }
                    },
                    {
                        "filename": "text_plus",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 39,
                            "h": 38
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 39,
                            "h": 38
                        },
                        "frame": {
                            "x": 313,
                            "y": 361,
                            "w": 39,
                            "h": 38
                        }
                    },
                    {
                        "filename": "text_0_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 32,
                            "h": 37
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 32,
                            "h": 37
                        },
                        "frame": {
                            "x": 356,
                            "y": 361,
                            "w": 32,
                            "h": 37
                        }
                    },
                    {
                        "filename": "text_2_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 29,
                            "h": 37
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 29,
                            "h": 37
                        },
                        "frame": {
                            "x": 370,
                            "y": 223,
                            "w": 29,
                            "h": 37
                        }
                    },
                    {
                        "filename": "text_8_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 31,
                            "h": 37
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 31,
                            "h": 37
                        },
                        "frame": {
                            "x": 403,
                            "y": 2,
                            "w": 31,
                            "h": 37
                        }
                    },
                    {
                        "filename": "text_1_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 23,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 23,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 43,
                            "w": 23,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_4_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 83,
                            "w": 30,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_9_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 123,
                            "w": 30,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_7_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 30,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 30,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 163,
                            "w": 30,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_5_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 29,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 29,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 203,
                            "w": 29,
                            "h": 36
                        }
                    },
                    {
                        "filename": "icon_bullet_empty_short",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 36
                        },
                        "frame": {
                            "x": 403,
                            "y": 243,
                            "w": 21,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_6_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 28,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 28,
                            "h": 36
                        },
                        "frame": {
                            "x": 370,
                            "y": 264,
                            "w": 28,
                            "h": 36
                        }
                    },
                    {
                        "filename": "icon_bullet_silver_short",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 36
                        },
                        "frame": {
                            "x": 402,
                            "y": 283,
                            "w": 21,
                            "h": 36
                        }
                    },
                    {
                        "filename": "icon_bullet_gold_short",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 36
                        },
                        "frame": {
                            "x": 370,
                            "y": 304,
                            "w": 21,
                            "h": 36
                        }
                    },
                    {
                        "filename": "text_3_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 28,
                            "h": 36
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 28,
                            "h": 36
                        },
                        "frame": {
                            "x": 395,
                            "y": 323,
                            "w": 28,
                            "h": 36
                        }
                    },
                    {
                        "filename": "icon_duck",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 34,
                            "h": 33
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 34,
                            "h": 33
                        },
                        "frame": {
                            "x": 392,
                            "y": 363,
                            "w": 34,
                            "h": 33
                        }
                    },
                    {
                        "filename": "icon_target",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 33,
                            "h": 33
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 33,
                            "h": 33
                        },
                        "frame": {
                            "x": 2,
                            "y": 409,
                            "w": 33,
                            "h": 33
                        }
                    },
                    {
                        "filename": "text_dots_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 21,
                            "h": 32
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 21,
                            "h": 32
                        },
                        "frame": {
                            "x": 39,
                            "y": 409,
                            "w": 21,
                            "h": 32
                        }
                    },
                    {
                        "filename": "text_cross_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 32,
                            "h": 32
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 32,
                            "h": 32
                        },
                        "frame": {
                            "x": 64,
                            "y": 408,
                            "w": 32,
                            "h": 32
                        }
                    },
                    {
                        "filename": "text_plus_small",
                        "rotated": false,
                        "trimmed": false,
                        "sourceSize": {
                            "w": 28,
                            "h": 28
                        },
                        "spriteSourceSize": {
                            "x": 0,
                            "y": 0,
                            "w": 28,
                            "h": 28
                        },
                        "frame": {
                            "x": 100,
                            "y": 407,
                            "w": 28,
                            "h": 28
                        }
                    }
                ]
            }
        ],
        "meta": {
            "app": "http://free-tex-packer.com",
            "version": "0.6.7"
        }
    }

}


