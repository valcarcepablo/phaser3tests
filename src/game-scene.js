import Phaser from 'phaser';
import {
  getHudJson, getStallJson, getObjectsJson, getDucksJson,
} from './spritesheets';
import ducks from './assets/ducks.png';
import hudImg from './assets/Hud.png';
import stallImg from './assets/Stall.png';
import objectstImg from './assets/Objects.png';
import downWood from './assets/down-wood.png';
import font from './assets/font.png';
import fontXML from './assets/font.xml';
import Ducks from './duck';

export default class MyGame extends Phaser.Scene {
  constructor() {
    super();
    Phaser.Scene.call(this, { key: 'myGame' });
  }

  init() {
    this.myVariables = {
      bulletsLeft: 3,
      timeLeft: 60,
      recharging: false,
      scored: 0,
    };
  }

  preload() {
    this.load.image('downWood', downWood);
    this.load.atlas('huds', hudImg, getHudJson());
    this.load.atlas('stall', stallImg, getStallJson());
    this.load.atlas('objects', objectstImg, getObjectsJson());
    this.load.atlas('ducks', ducks, getDucksJson());
    this.load.bitmapFont('font', font, fontXML);
    //   this.load.audio('ShootSound',ShootSound);
  }

  create() {
    // const Shoot_sound = this.sound.add('ShootSound');
    const hudsTexture = this.textures.get('huds');
    const stallTexture = this.textures.get('stall');
    const objectsTexture = this.textures.get('objects');
    const hudsFrames = hudsTexture.getFrameNames();
    const stallFrames = stallTexture.getFrameNames();
    const objectsFrames = objectsTexture.getFrameNames();

    const woodBackground = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height / 2,
      1280,
      720,
      'stall',
      'bg_wood',
    );

    const leftTree = this.add.image(
      this.scale.width * 0.1,
      this.scale.height * 0.45,
      'stall',
      stallFrames[5],
    );

    const grass1 = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.51,
      1268,
      146,
      'stall',
      stallFrames[10],
    );

    const grass2 = this.add.tileSprite(
      this.scale.width * 0.3 + 9,
      this.scale.height * 0.46 - 2,
      250,
      100,
      'stall',
      stallFrames[9],
    );

    const grass3 = this.add.tileSprite(
      this.scale.width * 0.7 + 24,
      this.scale.height * 0.46 - 2,
      250,
      100,
      'stall',
      stallFrames[9],
    );

    const rightTree = this.add.image(
      this.scale.width * 0.9,
      this.scale.height * 0.5,
      'stall',
      stallFrames[6],
    );

    const targetRed = this.add.image(
      this.scale.width * 0.2,
      this.scale.height * 0.70,
      'objects',
      objectsFrames[4],
    )
      .setInteractive();

    const stickTargetRed = this.add.image(
      this.scale.width * 0.2,
      this.scale.height * 0.87,
      'objects',
      objectsFrames[16],
    );

    const targetYellow = this.add.image(
      this.scale.width * 0.8,
      this.scale.height * 0.7,
      'objects',
      objectsFrames[13],
    )
      .setInteractive();

    const stickTargetYellow = this.add.image(
      this.scale.width * 0.8,
      this.scale.height * 0.85,
      'objects',
      objectsFrames[16],
    );

    const water1 = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.62,
      1268,
      146,
      'stall',
      stallFrames[7],
    )
      .setDisplaySize(1500, 160);

    const stickDuck1 = this.add.image(
      this.scale.width * 0.2,
      this.scale.height * 0.71,
      'objects',
      objectsFrames[14],
    );

    const stickDuck2 = this.add.image(
      this.scale.width * 0.7,
      this.scale.height * 0.76,
      'objects',
      objectsFrames[15],
    );

    this.duck1 = new Ducks(
      this,
      this.scale.width * 0.2,
      this.scale.height * 0.55,
      'ducks',
      'duckmaster-duck 1-000.png',
    )
      .setInteractive();

    this.duck2 = new Ducks(
      this,
      this.scale.width * 0.7,
      this.scale.height * 0.62,
      'ducks',
      'duckmaster-duck 2-000.png',
    )
      .setInteractive()
      .setFlipX(true);

    const woodBotton = this.add.tileSprite(
      this.scale.width * 0.5,
      this.scale.height * 0.9,
      1280,
      180,
      'stall',
      stallFrames[2],
    );

    const water2 = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.72,
      1268,
      146,
      'stall',
      stallFrames[8],
    )
      .setDisplaySize(1500, 160);

    const downWood = this.add.image(
      this.scale.width / 2,
      this.scale.height * 0.9,
      'downWood',
    )
      .setDisplaySize(1800, 160);

    const courtainLeft = this.add.image(
      this.scale.width * 0.05,
      this.scale.height / 2,
      'stall',
      stallFrames[4],
    )
      .setDisplaySize(186, 612);

    const courtainRight = this.add.image(
      this.scale.width * 0.95,
      this.scale.height / 2,
      'stall',
      stallFrames[4],
    )
      .setFlipX(true)
      .setDisplaySize(186, 612);

    const stringLeft = this.add.image(
      this.scale.width * 0,
      this.scale.height / 2,
      'stall',
      stallFrames[15],
    );

    const stringRight = this.add.image(
      this.scale.width,
      this.scale.height / 2,
      'stall',
      stallFrames[15],
    );

    const cloud = this.add.image(
      this.scale.width * 0.8,
      this.scale.height * 0.12,
      'stall',
      stallFrames[14],
    );

    const courtainTop2 = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.08,
      1011,
      62,
      'stall',
      stallFrames[12],
    );

    const courtainTop = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.12,
      600,
      62,
      'stall',
      stallFrames[12],
    );

    const courtainStraight = this.add.tileSprite(
      this.scale.width / 2,
      this.scale.height * 0.05,
      1288,
      80,
      'stall',
      stallFrames[11],
    );

    const crosshair = this.add.image(
      this.scale.width * 0.5,
      this.scale.height * 0.5,
      'huds',
      hudsFrames[16],
    );

    const weapon = this.add.image(
      this.scale.width * 0.7,
      this.scale.height * 0.8,
      'objects',
      objectsFrames[1],
    );

    const bulletsLeft1 = this.add.image(
      this.scale.width * 0.8,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[24],
    );

    const bulletsLeft2 = this.add.image(
      this.scale.width * 0.85,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[24],
    );

    const bulletsLeft3 = this.add.image(
      this.scale.width * 0.9,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[24],
    );

    const bullet1 = this.add.image(
      this.scale.width * 0.8,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[22],
    );

    const bullet2 = this.add.image(
      this.scale.width * 0.85,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[22],
    );

    const bullet3 = this.add.image(
      this.scale.width * 0.9,
      this.scale.height * 0.9,
      'huds',
      hudsFrames[22],
    );

    this.score = this.add.bitmapText(
      this.scale.width * 0.04,
      this.scale.height * 0.1,
      'font',
      this.myVariables.scored,
    )
      .setScale(1)
      .setFontSize(64);

    this.timer = this.add.bitmapText(
      this.scale.width * 0.9,
      this.scale.height * 0.1,
      'font',
      this.myVariables.timeLeft,
    )
      .setScale(1)
      .setFontSize(64);

    this.input.setDefaultCursor('none');

    this.input.on('pointermove', function (pointer) {
      crosshair.x = pointer.x;
      crosshair.y = pointer.y;
      crosshair.x = Phaser.Math.Wrap(crosshair.x, 0, this.scale.width);
      crosshair.y = Phaser.Math.Wrap(crosshair.y, 0, this.scale.height);
      weapon.x = crosshair.x + 200;
      weapon.y = crosshair.y + 200;
    }, this);

    this.duck1.moveDuck(1000, 320, 2500, 400);
    this.duck2.moveDuck(150, 350, 2000, 400);

    this.tweens.add({
      targets: [stickDuck2],
      x: 150,
      duration: 2000,
      ease: 'Linear',
      yoyo: true,
      loop: -1,
      flipX: true,
      repeat: -1,
    });

    this.tweens.add({
      targets: [stickDuck2],
      y: '445',
      duration: 400,
      ease: 'Sine.easeInOut',
      yoyo: true,
      loop: -1,
    });

    this.tweens.add({
      targets: [stickDuck1],
      y: '435',
      duration: 400,
      ease: 'Sine.easeInOut',
      yoyo: true,
      loop: -1,
    });

    this.tweens.add({
      targets: [stickDuck1],
      x: 1000,
      duration: 2500,
      ease: 'Linear',
      yoyo: true,
      loop: -1,
      flipX: true,
      repeat: -1,
    });

    const targetMove = (targets, delay) => {
      const targetMoveTw = this.tweens.add({
        targets,
        y: '-=250',
        duration: 1400,
        ease: 'Sine.easeInOut',
        yoyo: true,
        delay,
        onComplete: () => {
          const randomX = Phaser.Math.Between(100, 1000);
          targets.forEach((target) => {
            target.x = randomX;
          });
          targetMove(targets, delay);
        },
      });
    };

    const waves = (targets, delay, moveY, moveX) => {
      const targetMoveTw = this.tweens.add({
        targets,
        y: moveY,
        x: moveX,
        duration: 600,
        ease: 'Sine.easeInOut',
        yoyo: true,
        delay,
        loop: -1,
      });
    };

    targetMove([targetYellow, stickTargetYellow], 5000);
    targetMove([targetRed, stickTargetRed], 3000);
    waves([water1], 0, '430', '600');
    waves([water2], 44, '500', '680');

    const shoot = () => {
      if (this.myVariables.bulletsLeft === 3) {
        bullet3.setVisible(false);
        this.myVariables.bulletsLeft--;
      } else if (this.myVariables.bulletsLeft === 2) {
        bullet2.setVisible(false);
        this.myVariables.bulletsLeft--;
      } else if (this.myVariables.bulletsLeft === 1) {
        bullet1.setVisible(false);
        this.myVariables.bulletsLeft--;
      } else { /* empty */ }
    };

    const BulletsReload = () => {
      this.myVariables.recharging = true;
      switch (this.myVariables.bulletsLeft) {
        case 0:
          const ejemploTimer = this.time.addEvent({
            callback: () => {
              bullet1.setVisible(true);
              const ejemploTimer = this.time.addEvent({
                delay: 500,
                callback: () => {
                  bullet2.setVisible(true);
                  const ejemploTimer2 = this.time.addEvent({
                    delay: 500,
                    callback: () => {
                      bullet3.setVisible(true);
                      this.myVariables.bulletsLeft = 3;
                      const ejemploTimer = this.time.addEvent({
                        delay: 500,
                        callback: () => {
                          this.myVariables.recharging = false;
                        },
                      });
                    },
                  });
                },
              });
            },
          });
          break;

        case 1:
          const ejemploTimer2 = this.time.addEvent({
            callback: () => {
              bullet2.setVisible(true);
              const ejemploTimer2 = this.time.addEvent({
                delay: 500,
                callback: () => {
                  bullet3.setVisible(true);
                  this.myVariables.bulletsLeft = 3;
                  const ejemploTimer = this.time.addEvent({
                    delay: 500,
                    callback: () => {
                      this.myVariables.recharging = false;
                    },
                  });
                },
              });
            },
          });
          break;

        case 2:
          const ejemploTimer3 = this.time.addEvent({
            callback: () => {
              bullet3.setVisible(true);
              this.myVariables.bulletsLeft = 3;
              const ejemploTimer = this.time.addEvent({
                delay: 500,
                callback: () => {
                  this.myVariables.recharging = false;
                },
              });
            },
          });
          break;
      }
    };

    this.addPoints = (pointer) => {
      this.myVariables.scored += 20;
      const tenPoints = this.add.bitmapText(
        pointer.x,
        pointer.y - 100,
        'font',
        +20,
      );

      this.time.addEvent({
        delay: 1000,
        callback: () => {
          tenPoints.destroy();
        },
      });
    };

    const addExtraPoints = (pointer) => {
      this.myVariables.scored += 30;
      const x3Points = this.add.bitmapText(
        pointer.x,
        pointer.y - 100,
        'font',
        +30,
      );

      this.time.addEvent({
        delay: 1000,
        callback: () => {
          x3Points.destroy();
        },
      });
    };

    const getRechargingState = () => this.myVariables.recharging;

    const getBulletsLeft = () => this.myVariables.bulletsLeft;

    this.input.on('pointerdown', (pointer) => {
      if (getRechargingState() === true) {

      } else if (pointer.leftButtonDown() && getBulletsLeft() > 0) {
        // Shoot_sound.play();
        shoot();
      } else if (pointer.middleButtonDown()) {
        BulletsReload();
      }
    });
    this.duck1.shootDuck1();
    this.duck2.shootDuck2();

    targetRed.on('pointerdown', (pointer) => {
      if (getRechargingState() === false && getBulletsLeft() > 0) {
        addExtraPoints(pointer);
      }
    });

    targetYellow.on('pointerdown', (pointer) => {
      if (getRechargingState() === false && getBulletsLeft() > 0) {
        addExtraPoints(pointer);
      }
    });
  }

  update() {
    this.score.text = this.myVariables.scored;

    if (this.myVariables.timeLeft > 1) {
      this.myVariables.timeLeft -= 0.01;
      this.timer.text = this.myVariables.timeLeft.toFixed(0);
    } else {
      const time_up = this.add.image(
        this.scale.width * 0.5,
        this.scale.height * 0.3,
        'huds',
        'text_timeup',
      );

      const ejemploTimer = this.time.addEvent({
        delay: 1000,
        callback: () => {
          this.scene.start('Menu', { scored: this.myVariables.scored });
        },
      });
    }

    
    if (Phaser.Input.Keyboard.JustDown(this.input.keyboard.addKey
    (Phaser.Input.Keyboard.KeyCodes.ESC))) {
      this.input.setDefaultCursor('default');
      this.scene.start(
        'Menu',
        { scored: this.myVariables.scored },
      );
    }
  }
}
