import Phaser from 'phaser';
import menuScene from './menu-scene';
import gameScene from './game-scene';

class Game extends Phaser.Game {
  constructor() {
    super();
    const game = new Phaser.Game(config);
    this.scene.add('Menu', menuScene);
    this.scene.add('MyGame', gameScene);
    this.scene.start('Menu');
  }
}
const config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  scale:
    {
      width: 1280,
      height: 720,
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
    },
  scene: [menuScene, gameScene],
};
const game = new Phaser.Game(config);
