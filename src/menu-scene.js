import Phaser from 'phaser';
import menuBackground from './assets/MenuBackground.png';
import { getHudJson, getStallJson, getObjectsJson } from './spritesheets';
import hudImg from './assets/Hud.png';
import stallImg from './assets/Stall.png';
import objectstImg from './assets/Objects.png';
import font from './assets/font.png';
import fontXML from './assets/font.xml';

export default class Menu extends Phaser.Scene
{
  constructor ()
  {
    super();
    Phaser.Scene.call(this, { key: 'Menu' });
  }


  init(data) 
  {
    this.score = data.scored;
  }

  preload() 
  {
    this.load.image('menuBackground', menuBackground);
    this.load.atlas('huds', hudImg, getHudJson());
    this.load.atlas('stall', stallImg, getStallJson());
    this.load.atlas('objects', objectstImg, getObjectsJson());
    this.load.bitmapFont('font', font, fontXML);
  }

  create() 
  {
    const menu_background = this.add.image
    (
      this.scale.width * 0.5,
      this.scale.height * 0.5,
      'menuBackground',
    )
      .setDisplaySize(1280, 720);

    const ready = this.add.image
    (
      this.scale.width * 0.2,
      this.scale.height * 0.4,
      'huds',
      'text_ready',
    ).setInteractive();

    const go = this.add.image
    (
      this.scale.width * 0.2,
      this.scale.height * 0.4,
      'huds',
      'text_go',
    ).setInteractive().setVisible(false);

    ready.on('pointerover', () => {
      ready.setVisible(false);
      go.setVisible(true);
    });

    go.on('pointerout', () => {
      ready.setVisible(true);
      go.setVisible(false);
    });

    go.on('pointerup',function()
    {
      this.scene.start('myGame');
    }, this);

    this.score = this.add.bitmapText
    (
      this.scale.width * 0.82,
      this.scale.height * 0.5,
      'font',
      this.score,
    )
      .setScale(1)
      .setFontSize(64);
  }
}
