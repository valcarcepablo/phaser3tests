/* eslint-disable no-empty */
import Phaser from 'phaser';

export default class Ducks extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, asset, frame) {
    super(scene, x, y, asset, frame);
    this.setTexture(asset, frame);
    this.setPosition(x, y);
    scene.add.existing(this);

    this.initialize();
  }

  initialize() {
    this.scene.anims.create({
      key: 'duck2Animation',
      repeat: 2,
      yoyo: true,
      frames: this.scene.anims.generateFrameNames(
        'ducks',
        {
          prefix: 'duckmaster-duck 2-',
          suffix: '.png',
          end: 9,
          zeroPad: 3,
        },
      ),
    });

    this.scene.anims.create({
      key: 'duck1Animation',
      repeat: 2,
      yoyo: true,
      frames: this.scene.anims.generateFrameNames(
        'ducks',
        {
          prefix: 'duckmaster-duck 1-',
          suffix: '.png',
          end: 11,
          zeroPad: 3,
        },
      ),
    });
  }

  shootDuck1() {
    this.on('pointerdown', (pointer) => {
      if (this.scene.myVariables.recharging === false && this.scene.myVariables.bulletsLeft > 0) {
        if (this.anims.isPlaying) {

        } else {
          this.play('duck1Animation');
          this.scene.addPoints(pointer);
        }
      }
    });
  }

  shootDuck2() {
    this.on('pointerdown', (pointer) => {
      if (this.scene.myVariables.recharging === false && this.scene.myVariables.bulletsLeft > 0) {
        if (this.anims.isPlaying) {

        } else {
          this.play('duck2Animation');
          this.scene.addPoints(pointer);
        }
      }
    });
  }

  moveDuck(x, y, durationX, durationY) {
    this.scene.tweens.add({
      targets: this,
      x,
      duration: durationX,
      ease: 'Linear',
      yoyo: true,
      loop: -1,
      flipX: true,
      repeat: -1,
    });

    this.scene.tweens.add({
      targets: this,
      y,
      duration: durationY,
      ease: 'Sine.easeInOut',
      yoyo: true,
      loop: -1,
    });
  }
}
